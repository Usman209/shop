import React, { createContext, useState } from 'react'

import a from "../assets/1.jpg";
import b from "../assets/1.1.jpg";
import c from "../assets/1.2.jpg";
import d from "../assets/1.3.jpg";
import f from "../assets/2.jpg";
import g from "../assets/3.jpg";


export const ProductsContext = createContext();





const ProductContextProvider = (props) =>
{
  
  const [products] = useState( [
    { id: 1, name:"a" ,price:400, image:a,status:"hot"},
    { id: 2, name:"b" ,price:400, image:b,status:"hot"},
    { id: 3, name:"c" ,price:400, image:c,status:"hot"},
    { id: 4, name:"d" ,price:400, image:d,status:"new"},
    { id: 5, name:"f" ,price:400, image:f,status:"hot"},
    { id: 6, name:"g" ,price:400, image:g,status:"new"}
  ])
  return (
    <div>
      <ProductsContext.Provider value={ { products: [ ...products ] } }>
        
        {props.children}
      </ProductsContext.Provider>

    </div>
  )
}

export default ProductContextProvider
