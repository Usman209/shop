import React, { createContext, useReducer } from "react";

import { CartReducer } from "./CartReducer";

export const CartsContext = createContext();

const CartContext = (props) =>{
  const [cart, dispatch] = useReducer(CartReducer, {
    shoppingCart: [],
    totalPrice: 0,
    qty: 0
  });

  return (
    <CartsContext.Provider value={{...cart, dispatch }}>
      {props.children}
    </CartsContext.Provider>
  );
}

export default CartContext;
