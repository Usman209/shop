import React ,{useContext} from 'react'
import { CartsContext } from "../Global/CartContext";

function Cart ()
{
  const {shoppingCart,totalPrice,qty,dispatch} = useContext(CartsContext);
  return (
    <div className="cart-container">
      <div className="cart-details" style={{ marginTop: "100px" }}>
        { shoppingCart.length > 0 ?
          shoppingCart.map( ( cart ) => (
              <div className="cart" key={cart.id}>
                <span className="cart-image">
                  <img src={cart.image} alt="Not Found" />{" "}
                </span>
                <span className="cart-product-name">{cart.name}</span>
                <span className="cart-product-price">{cart.price}</span>
                <span className="inc">
                  <i className="fas fa-plus-square"></i>
                </span>
                <span className="dec">
                  <i className="fas fa-minus-square"></i>
                </span>
                <span className="product-quantity">{cart.qty}</span>
                <span className="product-total-price">400.00</span>
                <span className="delete-product">
                  <i className="fas fa-trash-alt"></i>{" "}
              </span>
              
              </div>
            ))
          : "Your cart currently is empty "}
      </div>
    </div>
  );
}

export default Cart
