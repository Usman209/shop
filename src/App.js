import React from "react";
import "./App.css";
import Navbar from './components/Navbar'
import ProductContextProvider from './Global/ProductContext'
import CartContext from './Global/CartContext'
import Products from './components/Products'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Cart from './components/Cart'
import NotFound from "./components/NotFound";

function App() {
  return (
    <div>
      <ProductContextProvider>
        <CartContext>
          <Router>
            <Navbar />
            <Switch>
              <Route path="/" exact component={Products} />
              <Route path="/cart" exact component={Cart} />
              <Route component={NotFound} />
            </Switch>
          </Router>
        </CartContext>
      </ProductContextProvider>
    </div>
  );
}


export default App;
